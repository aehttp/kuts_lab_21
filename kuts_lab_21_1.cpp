/* file name: kuts_lab_21_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.04.2022
*дата останньої зміни 16.04.2022
*лабораторна №21 
*варіант : №4
*/

#include <graphics.h>
#include <conio.h>
#include <windows.h>
#include <iostream>
#include <math.h>

using namespace std;

main() {
	
	float R, h;
	float V, a, r, b, P;
	cout << " a = ";
	cin >> a;
	cout << " R = ";
	cin >> R;
	cout << " b = ";
	cin >> b;
	
	h = a/2;
	r = pow(b,2) - pow(h,2);
	P = 3.14;
	V = 2/3*P*h*(pow(R,2)+R*r+pow(r,2))+4/3*P*pow(R,3);
	
	char V1[30];
	char a1[30];
	char R1[30];
	char b1[30];
	
	sprintf(V1,"%5.4f", V);
	sprintf(a1,"%5.0f", a);
	sprintf(R1,"%5.0f", R);
	sprintf(b1,"%5.0f", b);

	
	initwindow(800, 800);
	
	int i;
	
	for(i = 0; i < 10; i ++) 
	{
		arc(200, 300, 2, 180, 100);
		ellipse(200, 300, 150, 150, 100, 50);
		line(100, 300, 165, 450);
		line(300, 300, 235, 450);
		line(235, 450, 300, 605);
		line(165, 450, 100, 605);
		ellipse(200, 605, 150, 150, 100, 50);
		arc(200, 605, 180, 2, 100);
	}
	
	for(i = 0; i < 4; i ++) 
	{
		setlinestyle(i, 0, 1);
		ellipse(200, 450, 80, 80, 35, 15);
		line(100, 300, 300, 300);
		line(100, 605, 300, 605);
		line(200, 605, 200, 300);
	}
	setusercharsize(2, 1, 2, 1);
	
	setcolor(3);
	outtextxy(400, 370, " a =");
	outtextxy(460, 370, a1);
	
	outtextxy(400, 390, " R =");
	outtextxy(460, 390, R1);
	
	outtextxy(400, 410, " b =");
	outtextxy(460, 410, b1);
	
	outtextxy(400, 430, " V =");
	outtextxy(460, 430, V1);

	
	setcolor(5);
	outtextxy(202, 370, " a ");
	outtextxy(275, 530, " b ");
	outtextxy(205, 580, " R ");
getch();
}

