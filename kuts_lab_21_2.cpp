/* file name: kuts_lab_21_2
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.04.2022
*дата останньої зміни 16.04.2022
*лабораторна №21 
*варіант : №4
*/

#include <cmath>
#include <iostream>
#include <graphics.h>

const int WIDTH = 1500, HEIGHT = 800;

void drawLine(int moveToX, int moveToY, int drawX, int drawY, int color, int textX, int textY, char *name) {
  moveto(moveToX, moveToY);
  setcolor(color);
  lineto(drawX, drawY);
  outtextxy(textX, textY, name);
}

int main () {
  double x, y;
  initwindow(WIDTH, HEIGHT);
  moveto(WIDTH / 2, HEIGHT / 2); 
  
  for(x = -1000; x < WIDTH; x += 0.01) {
    y = 0.5 * sin(x);
    setcolor(1);
    lineto(WIDTH / 2 + (x * 100), HEIGHT / 2 - (y * 100));
  }
  
  setusercharsize(2, 1, 2, 1);
  outtextxy(800, 300, "y = 0.5 * sin(x)"); 
  drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2, WHITE, WIDTH, HEIGHT / 2 + 10, "X");
  drawLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT, WHITE, WIDTH / 2 + 10, 0, "Y");
  getch();
  closegraph();
  
  return 0;
}

